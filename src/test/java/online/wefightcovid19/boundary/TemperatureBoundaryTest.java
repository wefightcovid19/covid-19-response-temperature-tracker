package online.wefightcovid19.boundary;

import io.quarkus.test.junit.QuarkusTest;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@Disabled
@QuarkusTest
public class TemperatureBoundaryTest {

    @Test
    public void testHelloEndpoint() {
        given()
          .when().get("/temperature")
          .then()
             .statusCode(200)
             .body(is("temperature"));
    }

}