package online.wefightcovid19.boundary;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeTemperatureBoundaryIT extends TemperatureBoundaryTest {

    // Execute the same tests but in native mode.
}