INSERT INTO temperature_tracker.place VALUES ('1ca30e39-fc15-45a2-a78d-b0170c594dca', 'COUNTRY', -66.8854170000000039, 'United States', 'ChIJCzYy5IS16lQRQrfeQ5K5Oxw', 37.0902400000000014, -95.7128909999999991, 71.5388001000000031, 18.7762999999999991, 170.595699999999994, NULL);
INSERT INTO temperature_tracker.place VALUES ('4a28c82e-df93-43a2-9106-a6e8a6afaf03', 'ADMINISTRATIVE_AREA_LEVEL_1', -103.001964000000001, 'New Mexico, USA', 'ChIJqVKY50NQGIcRup41Yxpuv0Y', 34.5199402000000006, -105.870090099999999, 37.0002931000000004, 31.3322529000000003, -109.050173000000001, '1ca30e39-fc15-45a2-a78d-b0170c594dca');
INSERT INTO temperature_tracker.place VALUES ('930a62a0-0fad-4052-9145-38ce6082f068', 'ADMINISTRATIVE_AREA_LEVEL_2', -106.149574999999999, 'Bernalillo County, NM, USA', 'ChIJUX79BuYMIocR61XjFX3dWVg', 35.0177854000000011, -106.629130399999994, 35.2196390000000008, 34.869023900000002, -107.196169900000001, '4a28c82e-df93-43a2-9106-a6e8a6afaf03');
INSERT INTO temperature_tracker.place VALUES ('8c0662c5-679b-4e03-abef-7c3e52792aa1', 'LOCALITY', -106.471162899999996, 'Albuquerque, NM, USA', 'ChIJe4MJ090KIocR_fbZuM7408A', 35.0843859000000009, -106.650422000000006, 35.218053900000001, 34.9467659000000026, -106.881795999999994, '930a62a0-0fad-4052-9145-38ce6082f068');
INSERT INTO temperature_tracker.place VALUES ('a2265c85-10de-4da3-a0b9-20aea348d4b4', 'NEIGHBORHOOD', -106.479988899999995, 'Vista Del Mundo, Albuquerque, NM, USA', 'ChIJYZ6DnLKhGIcRpimurCLS8IM', 35.0903741999999994, -106.483289299999996, 35.0971358999999978, 35.0862680999999981, -106.491308000000004, '8c0662c5-679b-4e03-abef-7c3e52792aa1');

INSERT INTO temperature_tracker.profile VALUES ('c0eb21d0-955d-419b-95c0-2762f93ba896', 38, 'test.user@covid19response.online', 'MALE', 'FAHRENHEIT', '8c0662c5-679b-4e03-abef-7c3e52792aa1');

INSERT INTO temperature_tracker.symptom(symptom_id, name, index) VALUES ('ebc5b106-fd6c-44c9-9180-6e3165b6241f', 'a temperature', 0);
INSERT INTO temperature_tracker.symptom(symptom_id, name, index) VALUES ('10a033b3-2744-491a-95d3-16f0497af8b1', 'a cough', 1);
INSERT INTO temperature_tracker.symptom(symptom_id, name, index) VALUES ('ffbfd06a-d55c-496b-b323-7d2d6c7b02f4', 'sore throat', 2);
INSERT INTO temperature_tracker.symptom(symptom_id, name, index) VALUES ('33629a8b-1b7d-4ba4-bb5a-683698290cbb', 'fast breathing', 3);
INSERT INTO temperature_tracker.symptom(symptom_id, name, index) VALUES ('3499310b-58d4-45aa-8d11-d4b2ba373377', 'chills', 4);
INSERT INTO temperature_tracker.symptom(symptom_id, name, index) VALUES ('7b963ef9-2692-486b-bcf9-5293cb426aa5', 'shaking with chills', 5);
INSERT INTO temperature_tracker.symptom(symptom_id, name, index) VALUES ('d822f43d-ce25-4c63-9349-bbe5869b2975', 'muscle pain', 6);
INSERT INTO temperature_tracker.symptom(symptom_id, name, index) VALUES ('82114ad5-a861-4aca-bd22-28d5a2b497d8', 'headache', 7);
INSERT INTO temperature_tracker.symptom(symptom_id, name, index) VALUES ('fb3cb11f-9237-42d2-88bc-c8d4bb614b4f', 'a loss of taste or smell', 8);
INSERT INTO temperature_tracker.symptom(symptom_id, name, index) VALUES ('83ac6b76-33bb-4702-be4e-a501319802fc', 'none of the above', 9);
