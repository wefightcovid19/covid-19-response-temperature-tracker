package online.wefightcovid19.factory;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.keycloak.admin.client.Keycloak;

public class KeycloakFactory {

  @Inject
  @ConfigProperty(name = "keycloak.server.url")
  String serverUrl;

  @Inject
  @ConfigProperty(name = "keycloak.application.username")
  String applicationUsername;

  @Inject
  @ConfigProperty(name = "keycloak.application.password")
  String applicationPassword;

  @Produces
  public Keycloak createKeycloak() {
      return Keycloak.getInstance(serverUrl, "covid-19-response", applicationUsername, applicationPassword, "temperature-tracker");
  }
}
