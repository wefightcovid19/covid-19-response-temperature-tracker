package online.wefightcovid19.factory;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import com.google.maps.GeoApiContext;

import org.eclipse.microprofile.config.inject.ConfigProperty;

public class GeoApiContextFactory {

  @Inject
  @ConfigProperty(name = "google.maps.apiKey")
  String apiKey;

  @Produces
  GeoApiContext createGeoApiContext() {
    return new GeoApiContext.Builder().apiKey(apiKey).build();
  }
}