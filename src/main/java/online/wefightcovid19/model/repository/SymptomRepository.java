package online.wefightcovid19.model.repository;

import java.util.UUID;
import java.util.stream.Stream;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import online.wefightcovid19.constant.Query;
import online.wefightcovid19.model.entity.SymptomEntity;

@Dependent
public class SymptomRepository {

  @Inject
  EntityManager em;

  @Transactional
  public SymptomEntity find(UUID id) {
    return em.find(SymptomEntity.class, id);
  }

  public Stream<SymptomEntity> findAll() {
    return em.createNamedQuery(Query.SYMPTOM_FIND_ALL, SymptomEntity.class)
      .getResultStream();
  }
}