package online.wefightcovid19.model.repository;

import java.time.LocalDateTime;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import online.wefightcovid19.constant.Query;
import online.wefightcovid19.model.entity.SymptomEntity;
import online.wefightcovid19.model.entity.TemperatureSymptomEntity;

@Dependent
public class TemperatureSymptomRepository {

  @Inject
  EntityManager em;

  @Transactional
  public void create(final TemperatureSymptomEntity entity) {
    em.persist(entity);
  }

  public Long count(SymptomEntity symptom, LocalDateTime start, LocalDateTime end, Double north, Double east, Double south, Double west) {
    return em.createNamedQuery(Query.SYMPTOM_FIND_COUNT_FROM_START_TO_END, Long.class)
      .setParameter("symptom", symptom)
      .setParameter("start", start)
      .setParameter("end", end)
      .setParameter("north", north)
      .setParameter("east", east)
      .setParameter("south", south)
      .setParameter("west", west)
      .getSingleResult();
  }
}
