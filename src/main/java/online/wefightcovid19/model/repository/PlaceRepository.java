package online.wefightcovid19.model.repository;

import java.util.UUID;
import java.util.stream.Stream;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import online.wefightcovid19.constant.Query;
import online.wefightcovid19.model.entity.PlaceEntity;

@Dependent
public class PlaceRepository {

  @Inject
  EntityManager em;

  @Transactional
  public void create(PlaceEntity entity) {
    em.persist(entity);
  }

  @Transactional
  public PlaceEntity update(PlaceEntity entity) {
    return em.merge(entity);
  }

  public PlaceEntity find(UUID id) {
    return em.find(PlaceEntity.class, id);
  }

  public PlaceEntity findByAddress(String address) {
    PlaceEntity result = null;
    var results = em.createNamedQuery(Query.PLACE_FIND_BY_FORMATTED_ADDRESS, PlaceEntity.class)
      .setParameter("address", address)
      .getResultList();

    if (!results.isEmpty()) {
      result = results.get(0);
    }

    return result;
  }

  public Stream<PlaceEntity> findByParent(PlaceEntity parent) {
    return em.createNamedQuery(Query.PLACE_FIND_BY_PARENT, PlaceEntity.class)
      .setParameter("parent", parent)
      .getResultStream();
  }
}