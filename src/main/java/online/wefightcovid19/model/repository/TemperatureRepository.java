package online.wefightcovid19.model.repository;

import java.time.LocalDateTime;
import java.util.stream.Stream;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import online.wefightcovid19.constant.Query;
import online.wefightcovid19.model.entity.TemperatureEntity;

@Dependent
public class TemperatureRepository {

  @Inject
  EntityManager em;

  @Transactional
  public void create(TemperatureEntity entity) {
    em.persist(entity);
  }

  public Stream<TemperatureEntity> findTemperatures(LocalDateTime start, LocalDateTime end, Double north, Double east, Double south, Double west) {
    return em.createNamedQuery(Query.TEMPERATURE_FIND_FROM_START_TO_END, TemperatureEntity.class)
      .setParameter("start", start)
      .setParameter("end", end)
      .setParameter("north", north)
      .setParameter("east", east)
      .setParameter("south", south)
      .setParameter("west", west)
      .getResultStream();
  }

  public Double findAverageTemperature(LocalDateTime start, LocalDateTime end, Double north, Double east, Double south, Double west) {
    return em.createNamedQuery(Query.TEMPERATURE_FIND_AVERAGE_FROM_START_TO_END, Double.class)
      .setParameter("start", start)
      .setParameter("end", end)
      .setParameter("north", north)
      .setParameter("east", east)
      .setParameter("south", south)
      .setParameter("west", west)
      .getSingleResult();
  }
}