package online.wefightcovid19.model.repository;

import java.util.UUID;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import online.wefightcovid19.model.entity.ProfileEntity;

@Dependent
public class ProfileRepository {
  
  @Inject
  EntityManager em;
  
  @Transactional
  public void create(final ProfileEntity entity) {
    em.persist(entity);
  }

  public ProfileEntity read(final UUID profileId) {
    return em.find(ProfileEntity.class, profileId);
  }

  @Transactional
  public ProfileEntity update(final ProfileEntity entity) {
    return em.merge(entity);
  }
}
