package online.wefightcovid19.model.entity;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import online.wefightcovid19.constant.enumeration.GenderEnum;
import online.wefightcovid19.constant.enumeration.TemperatureTypeEnum;

@Entity
@Table(name = "profile")
public class ProfileEntity {

  @Id
  @GeneratedValue(generator = "profile-uuid")
  @GenericGenerator(name = "profile-uuid", strategy = "uuid2")
  @Type(type = "pg-uuid")
  @Column(name = "profile_id")
  private UUID id;

  @Column(name = "email")
  private String email;

  @Column(name = "age")
  private Integer age;

  @Column(name = "gender")
  @Enumerated(EnumType.STRING)
  private GenderEnum gender;

  @Column(name = "temperature_unit")
  @Enumerated(EnumType.STRING)
  private TemperatureTypeEnum temperatureUnit;

  @OneToOne
  @JoinColumn(name = "place_id")
  private PlaceEntity place;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public GenderEnum getGender() {
    return gender;
  }

  public void setGender(GenderEnum gender) {
    this.gender = gender;
  }

  public TemperatureTypeEnum getTemperatureUnit() {
    return temperatureUnit;
  }

  public void setTemperatureUnit(TemperatureTypeEnum temperatureUnit) {
    this.temperatureUnit = temperatureUnit;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  public PlaceEntity getPlace() {
    return place;
  }

  public void setPlace(PlaceEntity place) {
    this.place = place;
  }
}
