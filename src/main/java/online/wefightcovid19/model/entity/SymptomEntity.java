package online.wefightcovid19.model.entity;

import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import online.wefightcovid19.constant.Query;

@Entity
@Table(name = "symptom")
@NamedQuery(name = Query.SYMPTOM_FIND_ALL, query = Query.SYMPTOM_FIND_ALL)
@NamedQuery(name = Query.SYMPTOM_FIND_COUNT_FROM_START_TO_END, query = Query.SYMPTOM_FIND_COUNT_FROM_START_TO_END)
public class SymptomEntity {

  @Id
  @GeneratedValue(generator = "symptom-uuid")
  @GenericGenerator(name = "symptom-uuid", strategy = "uuid2")
  @Type(type = "pg-uuid")
  @Column(name = "symptom_id")
  private UUID id;

  @Column(name = "name")
  private String name;

  @Column(name = "index")
  private Integer index;

  @OneToMany(mappedBy = "symptom")
  private List<TemperatureSymptomEntity> temperatures;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<TemperatureSymptomEntity> getTemperatures() {
    return temperatures;
  }

  public void setTemperatures(List<TemperatureSymptomEntity> temperatures) {
    this.temperatures = temperatures;
  }

  public void addTemperature(TemperatureSymptomEntity temperature) {
    this.temperatures.add(temperature);
  }

  public Integer getIndex() {
    return index;
  }

  public void setIndex(Integer index) {
    this.index = index;
  }
}
