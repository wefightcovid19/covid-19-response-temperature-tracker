package online.wefightcovid19.model.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import online.wefightcovid19.model.entity.embeddable.TemperatureSymptomId;

@Entity
@Table(name = "temperature_symptom")
public class TemperatureSymptomEntity {

  @EmbeddedId
  private TemperatureSymptomId id;

  @ManyToOne
  @MapsId("temperature_id")
  @JoinColumn(name = "temperature_id")
  private TemperatureEntity temperature;

  @ManyToOne
  @MapsId("symptom_id")
  @JoinColumn(name = "symptom_id")
  private SymptomEntity symptom;

  public TemperatureSymptomId getId() {
    return id;
  }

  public void setId(TemperatureSymptomId id) {
    this.id = id;
  }

  public TemperatureEntity getTemperature() {
    return temperature;
  }

  public void setTemperature(TemperatureEntity temperature) {
    this.temperature = temperature;
  }

  public SymptomEntity getSymptom() {
    return symptom;
  }

  public void setSymptom(SymptomEntity symptom) {
    this.symptom = symptom;
  }
}
