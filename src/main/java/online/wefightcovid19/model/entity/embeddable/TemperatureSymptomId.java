package online.wefightcovid19.model.entity.embeddable;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import online.wefightcovid19.model.entity.SymptomEntity;
import online.wefightcovid19.model.entity.TemperatureEntity;

@Embeddable
public class TemperatureSymptomId implements Serializable {

  private static final long serialVersionUID = 1L;

  @Column(name = "temperature_id")
  private UUID temperatureId;

  @Column(name = "symptom_id")
  private UUID symptomId;

  public TemperatureSymptomId(TemperatureEntity temperature, SymptomEntity symptom) {
    this.temperatureId = temperature.getId();
    this.symptomId = symptom.getId();
  } 

  public UUID getTemperatureId() {
    return temperatureId;
  }

  public void setTemperatureId(UUID temperatureId) {
    this.temperatureId = temperatureId;
  }

  public UUID getSymptomId() {
    return symptomId;
  }

  public void setSymptomId(UUID symptomId) {
    this.symptomId = symptomId;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((symptomId == null) ? 0 : symptomId.hashCode());
    result = prime * result + ((temperatureId == null) ? 0 : temperatureId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    TemperatureSymptomId other = (TemperatureSymptomId) obj;
    if (symptomId == null) {
      if (other.symptomId != null)
        return false;
    } else if (!symptomId.equals(other.symptomId))
      return false;
    if (temperatureId == null) {
      if (other.temperatureId != null)
        return false;
    } else if (!temperatureId.equals(other.temperatureId))
      return false;
    return true;
  }
}
