package online.wefightcovid19.model.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import online.wefightcovid19.constant.Query;
import online.wefightcovid19.constant.enumeration.TemperatureTypeEnum;

@Entity
@Table(name = "temperature")
@NamedQuery(name = Query.TEMPERATURE_FIND_FROM_START_TO_END, query = Query.TEMPERATURE_FIND_FROM_START_TO_END)
@NamedQuery(name = Query.TEMPERATURE_FIND_AVERAGE_FROM_START_TO_END, query = Query.TEMPERATURE_FIND_AVERAGE_FROM_START_TO_END)
public class TemperatureEntity {

  @Id
  @GeneratedValue(generator = "temperature-uuid")
  @GenericGenerator(name = "temperature-uuid", strategy = "uuid2")
  @Type(type = "pg-uuid")
  @Column(name = "temperature_id")
  private UUID id;

  @Column(name = "type")
  @Enumerated(EnumType.STRING)
  private TemperatureTypeEnum type;

  @Column(name = "temperature")
  private Double temperature;

  @Column(name = "latitude")
  private Double latitude;

  @Column(name = "longitude")
  private Double longitude;

  @Column(name = "creation_timestamp")
  private LocalDateTime creationDateTime;

  @OneToOne
  @JoinColumn(name = "profile_id")
  private ProfileEntity profile;

  @OneToOne
  @JoinColumn(name = "place_id")
  private PlaceEntity place;

  @OneToMany(mappedBy = "temperature")
  private List<TemperatureSymptomEntity> temperatureSymptoms;

  public TemperatureEntity() {
    temperatureSymptoms = new ArrayList<TemperatureSymptomEntity>();
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public TemperatureTypeEnum getType() {
    return type;
  }

  public void setType(TemperatureTypeEnum type) {
    this.type = type;
  }

  public Double getTemperature() {
    return temperature;
  }

  public void setTemperature(Double temperature) {
    this.temperature = temperature;
  }

  public Double getLongitude() {
    return longitude;
  }

  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }

  public Double getLatitude() {
    return latitude;
  }

  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }

  public LocalDateTime getCreationDateTime() {
    return creationDateTime;
  }

  public void setCreationDateTime(LocalDateTime creationDateTime) {
    this.creationDateTime = creationDateTime;
  }

  public ProfileEntity getProfile() {
    return profile;
  }

  public void setProfile(ProfileEntity profile) {
    this.profile = profile;
  }

  public PlaceEntity getPlace() {
    return place;
  }

  public void setPlace(PlaceEntity place) {
    this.place = place;
  }

  public List<TemperatureSymptomEntity> getTemperatureSymptoms() {
    return temperatureSymptoms;
  }

  public void setTemperatureSymptoms(List<TemperatureSymptomEntity> temperatureSymptoms) {
    this.temperatureSymptoms = temperatureSymptoms;
  }

  public void addTemperatureSymptom(TemperatureSymptomEntity temperatureSymptom) {
    temperatureSymptom.setTemperature(this);
    this.temperatureSymptoms.add(temperatureSymptom);
  }
}