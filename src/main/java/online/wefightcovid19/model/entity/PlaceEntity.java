package online.wefightcovid19.model.entity;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.google.maps.model.AddressType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import online.wefightcovid19.constant.Query;

@Entity
@Table(name = "place")
@NamedQuery(name = Query.PLACE_FIND_BY_PARENT, query = Query.PLACE_FIND_BY_PARENT)
@NamedQuery(name = Query.PLACE_FIND_BY_FORMATTED_ADDRESS, query = Query.PLACE_FIND_BY_FORMATTED_ADDRESS)
public class PlaceEntity {

  @Id
  @GeneratedValue(generator = "place-uuid")
  @GenericGenerator(name = "place-uuid", strategy = "uuid2")
  @Type(type = "pg-uuid")
  @Column(name = "place_id")
  private UUID id;

  @Column(name = "formatted_address")
  private String formattedAddress;

  @Column(name = "google_maps_place_id")
  private String googleMapsPlaceId;

  @Enumerated(EnumType.STRING)
  @Column(name = "address_type")
  private AddressType addressType;

  @Column(name = "north_bound")
  private Double northBound;

  @Column(name = "east_bound")
  private Double eastBound;

  @Column(name = "south_bound")
  private Double southBound;

  @Column(name = "west_bound")
  private Double westBound;

  @Column(name = "latitude")
  private Double latitude;

  @Column(name = "longitude")
  private Double longitude;

  @OneToOne
  @JoinColumn(name = "parent_id")
  private PlaceEntity parent;

  public UUID getId() {
    return id;
  }

  public void setId(final UUID id) {
    this.id = id;
  }

  public AddressType getAddressType() {
    return addressType;
  }

  public void setAddressType(AddressType addressType) {
    this.addressType = addressType;
  }

  public String getFormattedAddress() {
    return formattedAddress;
  }

  public void setFormattedAddress(final String formattedAddress) {
    this.formattedAddress = formattedAddress;
  }

  public String getGoogleMapsPlaceId() {
    return googleMapsPlaceId;
  }

  public void setGoogleMapsPlaceId(final String googleMapsPlaceId) {
    this.googleMapsPlaceId = googleMapsPlaceId;
  }

  public Double getWestBound() {
    return westBound;
  }

  public void setWestBound(Double westBound) {
    this.westBound = westBound;
  }

  public Double getSouthBound() {
    return southBound;
  }

  public void setSouthBound(Double southBound) {
    this.southBound = southBound;
  }

  public Double getEastBound() {
    return eastBound;
  }

  public void setEastBound(Double eastBound) {
    this.eastBound = eastBound;
  }

  public Double getNorthBound() {
    return northBound;
  }

  public void setNorthBound(Double northBound) {
    this.northBound = northBound;
  }

  public Double getLongitude() {
    return longitude;
  }

  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }

  public Double getLatitude() {
    return latitude;
  }

  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }

  public PlaceEntity getParent() {
    return parent;
  }

  public void setParent(PlaceEntity parent) {
    this.parent = parent;
  }
}