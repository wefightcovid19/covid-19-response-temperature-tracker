package online.wefightcovid19.exception.mapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import online.wefightcovid19.exception.ClaimNotFoundException;

@Provider
public class ClaimNotFoundExceptionMapper implements ExceptionMapper<ClaimNotFoundException> {

  @Override
  public Response toResponse(ClaimNotFoundException exception) {
    Response.Status httpStatus = Response.Status.FORBIDDEN;

    return Response.status(httpStatus).build();
  }
}
