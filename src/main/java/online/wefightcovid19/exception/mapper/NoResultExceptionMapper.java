package online.wefightcovid19.exception.mapper;

import javax.persistence.NoResultException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class NoResultExceptionMapper implements ExceptionMapper<NoResultException> {

  @Override
  public Response toResponse(NoResultException exception) {
    Response.Status httpStatus = Response.Status.NOT_FOUND;

    return Response.status(httpStatus).build();
  }
}
