package online.wefightcovid19.exception;

public class ClaimNotFoundException extends RuntimeException {
  private static final long serialVersionUID = 7062206258268082003L;
}
