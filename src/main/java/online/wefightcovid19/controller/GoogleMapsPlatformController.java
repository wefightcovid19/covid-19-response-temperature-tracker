package online.wefightcovid19.controller;

import java.util.List;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.AddressType;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;

@Dependent
public class GoogleMapsPlatformController {

  @Inject
  GeoApiContext context;

  public GeocodingResult[] reverseGeocode(Double lat, Double lng, AddressType... types) throws Exception {
    return GeocodingApi.newRequest(context).resultType(types).latlng(new LatLng(lat, lng)).await();
  }

  public AddressType extractAddressType(GeocodingResult geocoding, AddressType... types) {
    AddressType result = null;

    for(var type : geocoding.types) {
      if (List.of(types).contains(type)) {
        result = type;
      }
    }

    return result;
  }
}