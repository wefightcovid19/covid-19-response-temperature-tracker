package online.wefightcovid19.controller;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import io.smallrye.jwt.auth.principal.DefaultJWTCallerPrincipal;
import online.wefightcovid19.exception.ClaimNotFoundException;

@Dependent
public class HttpServletRequestController {

  @Inject
  HttpServletRequest httpRequest;

  public String getClaim(String name) {
    final var principal = getUserPrincipal();

    if (principal.containsClaim(name)) {
      return principal.getClaim(name).toString();
    } else {
      throw new ClaimNotFoundException();
    }
  }

  public String getSubject() {
    final var principal = getUserPrincipal();

    return principal.getSubject();
  }

  private DefaultJWTCallerPrincipal getUserPrincipal() {
    if (httpRequest.getUserPrincipal() instanceof DefaultJWTCallerPrincipal) {
      return (DefaultJWTCallerPrincipal) httpRequest.getUserPrincipal();
    } else {
      throw new RuntimeException();
    }
  }
}
