package online.wefightcovid19.controller;

import java.util.List;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.transaction.Transactional;

import com.google.maps.model.AddressType;

import online.wefightcovid19.assembler.PlaceAssembler;
import online.wefightcovid19.model.entity.PlaceEntity;
import online.wefightcovid19.model.entity.ProfileEntity;
import online.wefightcovid19.model.entity.TemperatureEntity;
import online.wefightcovid19.model.repository.PlaceRepository;

@Dependent
public class PlaceController {

  @Inject
  PlaceRepository repository;

  @Inject
  PlaceAssembler placeAssembler;

  @Inject
  GoogleMapsPlatformController googleMapsPlatformController;

  private final AddressType[] TYPES = {
    AddressType.NEIGHBORHOOD,
    AddressType.LOCALITY,
    AddressType.ADMINISTRATIVE_AREA_LEVEL_2,
    AddressType.ADMINISTRATIVE_AREA_LEVEL_1,
    AddressType.COUNTRY
  };

  @Transactional
  public void createHierarchy(final ProfileEntity profile, final Double lat, final Double lng) throws Exception {
    final var hierarchy = new PlaceEntity[TYPES.length];

    for (final var geocode : googleMapsPlatformController.reverseGeocode(lat, lng, TYPES)) {
      final var addressType = googleMapsPlatformController.extractAddressType(geocode, TYPES);
      var place = repository.findByAddress(geocode.formattedAddress);

      if (place == null) {
        place = placeAssembler.assemblePlaceEntity(geocode, addressType);
        repository.create(place);
      }

      if (addressType == AddressType.LOCALITY) {
        profile.setPlace(place);
      }

      insertPlaceIntoHierachy(hierarchy, place);
    }

    createHierarchy(hierarchy);
  }

  @Transactional
  public void createHierarchy(final TemperatureEntity temperarure, final Double lat, final Double lng) throws Exception {
    final var hierarchy = new PlaceEntity[TYPES.length];
    
    for (final var geocode : googleMapsPlatformController.reverseGeocode(lat, lng, TYPES)) {
      final var addressType = googleMapsPlatformController.extractAddressType(geocode, TYPES);
      var place = repository.findByAddress(geocode.formattedAddress);

      if (place == null) {
        place = placeAssembler.assemblePlaceEntity(geocode, addressType);
        repository.create(place);
      }

      if (addressType == AddressType.NEIGHBORHOOD) {
        temperarure.setPlace(place);
      }

      insertPlaceIntoHierachy(hierarchy, place);
    }

    createHierarchy(hierarchy);
  }

  private void createHierarchy(final PlaceEntity[] hierarchy) {
    for (var i = 0; i < hierarchy.length - 1; i++) {
      hierarchy[i].setParent(hierarchy[i + 1]);
    }
  }

  private void insertPlaceIntoHierachy(final PlaceEntity[] hierachy, final PlaceEntity place) {
    final var index = List.of(TYPES).indexOf(place.getAddressType());
    hierachy[index] = place;
  }
}
