package online.wefightcovid19.dto;

import java.util.UUID;

import online.wefightcovid19.constant.enumeration.GenderEnum;
import online.wefightcovid19.constant.enumeration.TemperatureTypeEnum;

public class ProfileDto {
  private UUID id;
  private Integer age;
  private GenderEnum gender;
  private TemperatureTypeEnum temperatureUnit;
  private PlaceDto reverseGeocode;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(final Integer age) {
    this.age = age;
  }

  public GenderEnum getGender() {
    return gender;
  }

  public void setGender(final GenderEnum gender) {
    this.gender = gender;
  }

  public TemperatureTypeEnum getTemperatureUnit() {
    return temperatureUnit;
  }

  public void setTemperatureUnit(TemperatureTypeEnum temperatureUnit) {
    this.temperatureUnit = temperatureUnit;
  }

  public PlaceDto getReverseGeocode() {
    return reverseGeocode;
  }

  public void setReverseGeocode(final PlaceDto reverseGeocode) {
    this.reverseGeocode = reverseGeocode;
  }
}