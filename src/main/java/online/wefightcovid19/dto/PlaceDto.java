package online.wefightcovid19.dto;

import java.util.UUID;

public class PlaceDto extends CreateReverseGeocodeDto {
  private UUID placeId;
  private String googleMapsPlaceId;
  private String formattedAddress;
  private Double northBound;
  private Double eastBound;
  private Double southBound;
  private Double westBound;
  private Double latitude;
  private Double longitude;

  public void setPlaceId(UUID placeId) {
    this.placeId = placeId;
  }

  public UUID getPlaceId() {
    return placeId;
  }

  public String getGoogleMapsPlaceId() {
    return googleMapsPlaceId;
  }

  public void setFormattedAddress(String formattedAddress) {
    this.formattedAddress = formattedAddress;
  }

  public String getFormattedAddress() {
    return formattedAddress;
  }

  public void setGoogleMapsPlaceId(String googleMapsPlaceId) {
    this.googleMapsPlaceId = googleMapsPlaceId;
  }

  public Double getWestBound() {
    return westBound;
  }

  public void setWestBound(Double westBound) {
    this.westBound = westBound;
  }

  public Double getEastBound() {
    return eastBound;
  }

  public void setEastBound(Double eastBound) {
    this.eastBound = eastBound;
  }

  public Double getSouthBound() {
    return southBound;
  }

  public void setSouthBound(Double southBound) {
    this.southBound = southBound;
  }

  public Double getNorthBound() {
    return northBound;
  }

  public void setNorthBound(Double northBound) {
    this.northBound = northBound;
  }

  public Double getLatitude() {
    return latitude;
  }

  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }

  public Double getLongitude() {
    return longitude;
  }

  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }
}