package online.wefightcovid19.dto;

import java.util.List;

import online.wefightcovid19.constant.enumeration.TemperatureTypeEnum;

public class CreateTemperatureDto {
  private Double temperature;
  private TemperatureTypeEnum type;
  private List<SymptomDto> symptoms;
  private CreateReverseGeocodeDto createReverseGeocode;

  public Double getTemperature() {
    return temperature;
  }

  public void setTemperature(final Double temperature) {
    this.temperature = temperature;
  }

  public TemperatureTypeEnum getType() {
    return type;
  }

  public void setType(TemperatureTypeEnum temperatureType) {
    this.type = temperatureType;
  }

  public CreateReverseGeocodeDto getCreateReverseGeocode() {
    return createReverseGeocode;
  }

  public void setCreateReverseGeocode(final CreateReverseGeocodeDto createReverseGeocode) {
    this.createReverseGeocode = createReverseGeocode;
  }

  public List<SymptomDto> getSymptoms() {
    return symptoms;
  }

  public void setSymptoms(List<SymptomDto> symptoms) {
    this.symptoms = symptoms;
  }
}