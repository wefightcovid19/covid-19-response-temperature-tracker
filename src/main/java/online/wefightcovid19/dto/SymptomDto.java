package online.wefightcovid19.dto;

import java.util.UUID;

public class SymptomDto {
  private UUID id;
  private String name;

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public UUID getId() {
    return id;
  }

  public void setId(final UUID id) {
    this.id = id;
  }
}