package online.wefightcovid19.dto;

import online.wefightcovid19.constant.enumeration.TemperatureTypeEnum;

public class TemperatureDto {
  private Double temperature;
  private TemperatureTypeEnum type;
  private Double latitude;
  private Double longitude;

  public Double getTemperature() {
    return temperature;
  }

  public void setTemperature(Double temperature) {
    this.temperature = temperature;
  }

  public TemperatureTypeEnum getType() {
    return type;
  }

  public void setType(TemperatureTypeEnum type) {
    this.type = type;
  }

  public Double getLongitude() {
    return longitude;
  }

  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }

  public Double getLatitude() {
    return latitude;
  }

  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }
}