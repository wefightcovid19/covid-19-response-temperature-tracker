package online.wefightcovid19.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import online.wefightcovid19.constant.enumeration.GenderEnum;
import online.wefightcovid19.constant.enumeration.TemperatureTypeEnum;

public class UpdateProfileDto {

  @NotNull
  @Min(0)
  @Max(125)
  private Integer age;

  @NotNull
  private GenderEnum gender;

  @NotNull
  private TemperatureTypeEnum temperatureUnit;

  private CreateReverseGeocodeDto createReverseGeocode;

  public Integer getAge() {
    return age;
  }

  public void setAge(final Integer age) {
    this.age = age;
  }

  public GenderEnum getGender() {
    return gender;
  }

  public void setGender(final GenderEnum gender) {
    this.gender = gender;
  }

  public TemperatureTypeEnum getTemperatureUnit() {
    return temperatureUnit;
  }

  public void setTemperatureUnit(TemperatureTypeEnum temperatureUnit) {
    this.temperatureUnit = temperatureUnit;
  }

  public CreateReverseGeocodeDto getCreateReverseGeocode() {
    return createReverseGeocode;
  }

  public void setCreateReverseGeocode(final CreateReverseGeocodeDto reverseGeocode) {
    this.createReverseGeocode = reverseGeocode;
  }
}
