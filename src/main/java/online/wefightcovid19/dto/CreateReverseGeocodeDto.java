package online.wefightcovid19.dto;

import javax.validation.constraints.NotNull;

public class CreateReverseGeocodeDto {

  @NotNull
  private Double latitude;

  @NotNull
  private Double longitude;

  public Double getLongitude() {
    return longitude;
  }

  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }

  public Double getLatitude() {
    return latitude;
  }

  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }
}