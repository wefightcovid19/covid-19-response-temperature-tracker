package online.wefightcovid19.constant;

public class Security {
  public static final String USER_ROLE_NAME = "user";
  public static final String COVID_REALM_NAME = "covid-19-response";
}
