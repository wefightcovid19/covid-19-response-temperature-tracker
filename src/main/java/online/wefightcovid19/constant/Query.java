package online.wefightcovid19.constant;

public class Query {
  public final static String PLACE_FIND_BY_PARENT = "SELECT p FROM PlaceEntity p WHERE p.parent = :parent ORDER BY p.formattedAddress";
  public final static String PLACE_FIND_BY_FORMATTED_ADDRESS = "SELECT p FROM PlaceEntity p WHERE p.formattedAddress = :address";
  public final static String TEMPERATURE_FIND_FROM_START_TO_END = "SELECT t FROM TemperatureEntity t WHERE (t.creationDateTime BETWEEN :start AND :end) AND (t.latitude BETWEEN :south AND :north) AND (t.longitude BETWEEN :west AND :east)";
  public final static String TEMPERATURE_FIND_AVERAGE_FROM_START_TO_END = "SELECT AVG(t.temperature) FROM TemperatureEntity t WHERE (t.creationDateTime BETWEEN :start AND :end) AND (t.latitude BETWEEN :south AND :north) AND (t.longitude BETWEEN :west AND :east)";
  public final static String SYMPTOM_FIND_COUNT_FROM_START_TO_END = "SELECT COUNT(ts) FROM TemperatureEntity t JOIN t.temperatureSymptoms ts WHERE ts.symptom = :symptom AND (t.creationDateTime BETWEEN :start AND :end) AND (t.latitude BETWEEN :south AND :north) AND (t.longitude BETWEEN :west AND :east)";
  public final static String SYMPTOM_FIND_ALL = "SELECT s FROM SymptomEntity s ORDER BY s.index";
}
