package online.wefightcovid19.constant.enumeration;

public enum GenderEnum {
  MALE, FEMALE
}