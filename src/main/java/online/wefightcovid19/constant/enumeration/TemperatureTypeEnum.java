package online.wefightcovid19.constant.enumeration;

public enum TemperatureTypeEnum {
  FAHRENHEIT, CELCIUS
}