package online.wefightcovid19.boundary;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.google.maps.model.AddressType;

import org.eclipse.microprofile.metrics.annotation.Timed;

import online.wefightcovid19.assembler.PlaceAssembler;
import online.wefightcovid19.controller.GoogleMapsPlatformController;
import online.wefightcovid19.dto.PlaceDto;
import online.wefightcovid19.model.repository.PlaceRepository;

@Timed
@Path("/place")
public class PlaceBoundary {
  
  @Inject
  PlaceAssembler assembler;
  
  @Inject
  PlaceRepository repository;

  @Inject
  GoogleMapsPlatformController googleMapsPlatformController;

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public List<PlaceDto> roots() {
    return repository.findByParent(null)
      .map(result -> assembler.assemblePlaceDto(result))
      .collect(Collectors.toList());
  }

  @GET
  @Path("/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public PlaceDto place(@PathParam("id") UUID id) {
    final var place = repository.find(id);
    final var dto = assembler.assemblePlaceDto(place);

    return dto;
  }

  @GET
  @Path("/{id}/children")
  @Produces(MediaType.APPLICATION_JSON)
  public List<PlaceDto> children(@PathParam("id") UUID id) {
    final var parent = repository.find(id);
    
    return repository.findByParent(parent)
      .map(result -> assembler.assemblePlaceDto(result))
      .collect(Collectors.toList());
  }

  @GET
  @Path("/geocode/reverse")
  @Produces(MediaType.APPLICATION_JSON)
  public List<PlaceDto> reverseGeocodeCity(@QueryParam("latitude") Double lat, @QueryParam("longitude") Double lng) throws Exception {
    return Stream.of(googleMapsPlatformController.reverseGeocode(lat, lng, AddressType.LOCALITY))
      .map(result -> assembler.assemblePlaceDto(result))
      .collect(Collectors.toList());
  }
}
