package online.wefightcovid19.boundary;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.metrics.annotation.Timed;

import online.wefightcovid19.assembler.TemperatureAssembler;
import online.wefightcovid19.assembler.TemperatureSymptomAssembler;
import online.wefightcovid19.constant.Security;
import online.wefightcovid19.controller.HttpServletRequestController;
import online.wefightcovid19.controller.PlaceController;
import online.wefightcovid19.dto.CreateTemperatureDto;
import online.wefightcovid19.dto.TemperatureDto;
import online.wefightcovid19.model.repository.ProfileRepository;
import online.wefightcovid19.model.repository.SymptomRepository;
import online.wefightcovid19.model.repository.TemperatureRepository;
import online.wefightcovid19.model.repository.TemperatureSymptomRepository;

@Timed
@RolesAllowed(Security.USER_ROLE_NAME)
@Path("/temperature")
public class TemperatureBoundary {

  @Inject
  TemperatureRepository repository;

  @Inject
  TemperatureAssembler assembler;

  @Inject
  ProfileRepository profileRepository;

  @Inject
  PlaceController placeController;

  @Inject
  SymptomRepository symptomRepository;

  @Inject
  TemperatureSymptomAssembler temperatureSymptomAssembler;

  @Inject
  TemperatureSymptomRepository temperatureSymptomRepository;

  @Inject
  HttpServletRequestController httpServletRequestController;

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public List<TemperatureDto> getTemperatures(
    @NotNull @QueryParam("start") String startString,
    @QueryParam("end") String endString,
    @NotNull @QueryParam("north") Double north,
    @NotNull @QueryParam("east") Double east,
    @NotNull @QueryParam("south") Double south,
    @NotNull @QueryParam("west") Double west
  ) {
    final var start = LocalDateTime.ofInstant(Instant.parse(startString), ZoneOffset.UTC);
    var end = LocalDateTime.now();

    if (endString != null) {
      end = LocalDateTime.ofInstant(Instant.parse(endString), ZoneOffset.UTC);
    }

    return repository.findTemperatures(start, end, north, east, south, west)
      .map(entity -> assembler.assembleTemperatureDto(entity))
      .collect(Collectors.toList());
  }

  @GET
  @Path("average")
  @Produces(MediaType.TEXT_PLAIN)
  public Double getAverageTemperature(
    @NotNull @QueryParam("start") String startString,
    @QueryParam("end") String endString,
    @NotNull @QueryParam("north") Double north,
    @NotNull @QueryParam("east") Double east,
    @NotNull @QueryParam("south") Double south,
    @NotNull @QueryParam("west") Double west
  ) {
    final var start = LocalDateTime.ofInstant(Instant.parse(startString), ZoneOffset.UTC);
    var end = LocalDateTime.now();

    if (endString != null) {
      end = LocalDateTime.ofInstant(Instant.parse(endString), ZoneOffset.UTC);
    }

    return repository.findAverageTemperature(start, end, north, east, south, west);
  }

  @Transactional
  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public TemperatureDto createTemperature(@Valid final CreateTemperatureDto createDto) throws Exception {
    final var profileId = UUID.fromString(httpServletRequestController.getClaim("profile_id"));
    final var profile = profileRepository.read(profileId);
    final var temperature = assembler.assembleTemperatureEntity(createDto, profile);
    repository.create(temperature);

    if (createDto.getCreateReverseGeocode() != null) {
      final var lat = createDto.getCreateReverseGeocode().getLatitude();
      final var lng = createDto.getCreateReverseGeocode().getLongitude();

      placeController.createHierarchy(temperature, lat, lng);
    }

    if (createDto.getSymptoms() != null) {
      createDto.getSymptoms().forEach(dto -> {
        final var symptom = symptomRepository.find(dto.getId());
        final var temperatureSymptom = temperatureSymptomAssembler.assembleTemperatureSymptomEntity(temperature, symptom);

        temperatureSymptomRepository.create(temperatureSymptom);
      });
    }

    final var dto = assembler.assembleTemperatureDto(temperature);

    return dto;
  }
}
