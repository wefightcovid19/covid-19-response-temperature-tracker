package online.wefightcovid19.boundary;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.metrics.annotation.Timed;

import online.wefightcovid19.assembler.SymptomAssembler;
import online.wefightcovid19.constant.Security;
import online.wefightcovid19.dto.SymptomDto;
import online.wefightcovid19.model.repository.SymptomRepository;
import online.wefightcovid19.model.repository.TemperatureSymptomRepository;

@Timed
@RolesAllowed(Security.USER_ROLE_NAME)
@Path("/symptom")
public class SymptomBoundary {

  @Inject
  TemperatureSymptomRepository temperatureSymptomRepository;

  @Inject
  SymptomRepository repository;

  @Inject
  SymptomAssembler assembler;

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public List<SymptomDto> getSymptoms() {
    return repository.findAll()
      .map(entity -> assembler.assembleSymptomDto(entity))
      .collect(Collectors.toList());
  }

  @GET
  @Path("/{id}/count")
  @Produces(MediaType.TEXT_PLAIN)
  public Long getSymptomCount(
    @PathParam("id") UUID symptomId,
    @NotNull @QueryParam("start") String startString,
    @QueryParam("end") String endString,
    @NotNull @QueryParam("north") Double north,
    @NotNull @QueryParam("east") Double east,
    @NotNull @QueryParam("south") Double south,
    @NotNull @QueryParam("west") Double west
  ) {
    final var symptom = repository.find(symptomId);
    final var start = LocalDateTime.ofInstant(Instant.parse(startString), ZoneOffset.UTC);
    var end = LocalDateTime.now();

    if (endString != null) {
      end = LocalDateTime.ofInstant(Instant.parse(endString), ZoneOffset.UTC);
    }

    return temperatureSymptomRepository.count(symptom, start, end, north, east, south, west);
  }
}
