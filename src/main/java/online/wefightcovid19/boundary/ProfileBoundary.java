package online.wefightcovid19.boundary;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.metrics.annotation.Timed;
import org.keycloak.admin.client.Keycloak;

import online.wefightcovid19.assembler.ProfileAssembler;
import online.wefightcovid19.constant.Security;
import online.wefightcovid19.controller.HttpServletRequestController;
import online.wefightcovid19.controller.PlaceController;
import online.wefightcovid19.dto.CreateProfileDto;
import online.wefightcovid19.dto.ProfileDto;
import online.wefightcovid19.dto.UpdateProfileDto;
import online.wefightcovid19.model.repository.PlaceRepository;
import online.wefightcovid19.model.repository.ProfileRepository;

@Timed
@Path("/profile")
public class ProfileBoundary {

  @Inject
  Keycloak keycloak;

  @Inject
  ProfileAssembler assembler;

  @Inject
  ProfileRepository repository;

  @Inject
  PlaceController placeController;

  @Inject
  PlaceRepository placeRepository;

  @Inject
  HttpServletRequestController httpServletRequestController;

  @Transactional
  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public ProfileDto createProfile(@Valid final CreateProfileDto createDto) throws Exception {
    final var subject = httpServletRequestController.getSubject();
    final var realmResource = keycloak.realm(Security.COVID_REALM_NAME);
    final var userResource = realmResource.users().get(subject);
    final var userRepresentation = userResource.toRepresentation();
    final var profile = assembler.assembleProfileEntity(createDto);
    profile.setEmail(userRepresentation.getEmail());

    if (createDto.getCreateReverseGeocode() != null) {
      final var lat = createDto.getCreateReverseGeocode().getLatitude();
      final var lng = createDto.getCreateReverseGeocode().getLongitude();

      placeController.createHierarchy(profile, lat, lng);
    }

    repository.create(profile);

    userRepresentation.singleAttribute("profileId", profile.getId().toString());

    userResource.update(userRepresentation);

    final var userRole = realmResource.roles().get(Security.USER_ROLE_NAME).toRepresentation();
    userResource.roles().realmLevel().add(List.of(userRole));

    final var dto = assembler.assembleProfileDto(profile);

    return dto;
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public ProfileDto getProfile() {
    final var profileId = UUID.fromString(httpServletRequestController.getClaim("profile_id"));
    final var profile = repository.read(profileId);
    ProfileDto dto = null;
    
    if (profile != null) {
      dto = assembler.assembleProfileDto(profile);
    }

    return dto;
  }

  @Transactional
  @PUT
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public ProfileDto updateProfile(@Valid final UpdateProfileDto updateDto) throws Exception {
    final var profileId = UUID.fromString(httpServletRequestController.getClaim("profile_id"));
    final var profile = assembler.assembleProfileEntity(updateDto, repository.read(profileId));

    if (updateDto.getCreateReverseGeocode() != null) {
      final var lat = updateDto.getCreateReverseGeocode().getLatitude();
      final var lng = updateDto.getCreateReverseGeocode().getLongitude();

      placeController.createHierarchy(profile, lat, lng);
    }

    final var dto = assembler.assembleProfileDto(profile);

    return dto;
  }
}
