package online.wefightcovid19.assembler;

import java.time.LocalDateTime;

import javax.enterprise.context.Dependent;

import online.wefightcovid19.dto.CreateTemperatureDto;
import online.wefightcovid19.dto.TemperatureDto;
import online.wefightcovid19.model.entity.ProfileEntity;
import online.wefightcovid19.model.entity.TemperatureEntity;

@Dependent
public class TemperatureAssembler {

  public TemperatureEntity assembleTemperatureEntity(final CreateTemperatureDto createDto, final ProfileEntity profile) {
    final var entity = new TemperatureEntity();
    entity.setType(createDto.getType());
    entity.setTemperature(createDto.getTemperature());
    entity.setCreationDateTime(LocalDateTime.now());
    entity.setLatitude(profile.getPlace().getLatitude());
    entity.setLongitude(profile.getPlace().getLongitude());
    entity.setProfile(profile);

    if (createDto.getCreateReverseGeocode() != null) {
      entity.setLatitude(createDto.getCreateReverseGeocode().getLatitude());
      entity.setLongitude(createDto.getCreateReverseGeocode().getLongitude());
    }

    return entity;
  }

  public TemperatureDto assembleTemperatureDto(final TemperatureEntity entity) {
    final var dto = new TemperatureDto();
    dto.setTemperature(entity.getTemperature());
    dto.setType(entity.getType());

    if (entity.getPlace() != null) {
      dto.setLatitude(entity.getPlace().getLatitude());
      dto.setLongitude(entity.getPlace().getLongitude());
    } else {
      dto.setLatitude(entity.getProfile().getPlace().getLatitude());
      dto.setLongitude(entity.getProfile().getPlace().getLatitude());
    }

    return dto;
  }
}