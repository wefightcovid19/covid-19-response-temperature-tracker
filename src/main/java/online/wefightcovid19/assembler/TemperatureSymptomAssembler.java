package online.wefightcovid19.assembler;

import javax.enterprise.context.Dependent;

import online.wefightcovid19.model.entity.SymptomEntity;
import online.wefightcovid19.model.entity.TemperatureEntity;
import online.wefightcovid19.model.entity.TemperatureSymptomEntity;
import online.wefightcovid19.model.entity.embeddable.TemperatureSymptomId;

@Dependent
public class TemperatureSymptomAssembler {
 
  public TemperatureSymptomEntity assembleTemperatureSymptomEntity(TemperatureEntity temperature, SymptomEntity symptom) {
    final var id = new TemperatureSymptomId(temperature, symptom);
    final var entity = new TemperatureSymptomEntity();
    entity.setId(id);
    entity.setTemperature(temperature);
    entity.setSymptom(symptom);

    return entity;
  }
}
