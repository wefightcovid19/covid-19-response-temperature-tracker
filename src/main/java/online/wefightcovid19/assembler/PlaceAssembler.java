package online.wefightcovid19.assembler;

import javax.enterprise.context.Dependent;

import com.google.maps.model.AddressType;
import com.google.maps.model.GeocodingResult;

import online.wefightcovid19.dto.PlaceDto;
import online.wefightcovid19.model.entity.PlaceEntity;

@Dependent
public class PlaceAssembler {

  public PlaceDto assemblePlaceDto(final GeocodingResult result) {
    final var dto = new PlaceDto();
    dto.setFormattedAddress(result.formattedAddress);
    dto.setGoogleMapsPlaceId(result.placeId);
    dto.setNorthBound(result.geometry.bounds.northeast.lat);
    dto.setEastBound(result.geometry.bounds.northeast.lng);
    dto.setSouthBound(result.geometry.bounds.southwest.lat);
    dto.setWestBound(result.geometry.bounds.southwest.lng);
    dto.setLatitude(result.geometry.location.lat);
    dto.setLongitude(result.geometry.location.lng);

    return dto;
  }

  public PlaceDto assemblePlaceDto(final PlaceEntity place) {
    final var dto = new PlaceDto();
    dto.setPlaceId(place.getId());
    dto.setGoogleMapsPlaceId(place.getGoogleMapsPlaceId());
    dto.setFormattedAddress(place.getFormattedAddress());
    dto.setNorthBound(place.getNorthBound());
    dto.setEastBound(place.getEastBound());
    dto.setSouthBound(place.getSouthBound());
    dto.setWestBound(place.getWestBound());
    dto.setLatitude(place.getLatitude());
    dto.setLongitude(place.getLongitude());

    return dto;
  }

  public PlaceEntity assemblePlaceEntity(final GeocodingResult result, final AddressType addressType) {
    final var entity = new PlaceEntity();
    entity.setAddressType(addressType);
    entity.setGoogleMapsPlaceId(result.placeId);
    entity.setFormattedAddress(result.formattedAddress);
    entity.setNorthBound(result.geometry.bounds.northeast.lat);
    entity.setEastBound(result.geometry.bounds.northeast.lng);
    entity.setSouthBound(result.geometry.bounds.southwest.lat);
    entity.setWestBound(result.geometry.bounds.southwest.lng);
    entity.setLatitude(result.geometry.location.lat);
    entity.setLongitude(result.geometry.location.lng);

    return entity;
  }
}