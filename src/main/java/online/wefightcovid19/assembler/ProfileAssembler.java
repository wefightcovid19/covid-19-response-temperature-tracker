package online.wefightcovid19.assembler;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import online.wefightcovid19.dto.CreateProfileDto;
import online.wefightcovid19.dto.ProfileDto;
import online.wefightcovid19.dto.UpdateProfileDto;
import online.wefightcovid19.model.entity.ProfileEntity;

@Dependent
public class ProfileAssembler {

  @Inject
  PlaceAssembler placeAssembler;

  public ProfileEntity assembleProfileEntity(final CreateProfileDto dto) {
    final var entity = new ProfileEntity();
    entity.setAge(dto.getAge());
    entity.setGender(dto.getGender());
    entity.setTemperatureUnit(dto.getTemperatureUnit());

    return entity;
  }

  public ProfileEntity assembleProfileEntity(final UpdateProfileDto dto, final ProfileEntity entity) {
    entity.setAge(dto.getAge());
    entity.setGender(dto.getGender());
    entity.setTemperatureUnit(dto.getTemperatureUnit());

    return entity;
  }

  public ProfileDto assembleProfileDto(final ProfileEntity entity) {
    final var reverseGeocodeDto = placeAssembler.assemblePlaceDto(entity.getPlace());

    final var dto = new ProfileDto();
    dto.setAge(entity.getAge());
    dto.setGender(entity.getGender());
    dto.setTemperatureUnit(entity.getTemperatureUnit());
    dto.setReverseGeocode(reverseGeocodeDto);
    
    return dto;
  }
}
