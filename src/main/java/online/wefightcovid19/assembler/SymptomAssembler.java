package online.wefightcovid19.assembler;

import javax.enterprise.context.Dependent;

import online.wefightcovid19.dto.SymptomDto;
import online.wefightcovid19.model.entity.SymptomEntity;

@Dependent
public class SymptomAssembler {

  public SymptomDto assembleSymptomDto(SymptomEntity entity) {
    final var dto = new SymptomDto();
    dto.setId(entity.getId());
    dto.setName(entity.getName());

    return dto;
  }
}