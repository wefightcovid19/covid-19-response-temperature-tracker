#!/bin/bash

cd ..
aws --profile covid-19-response ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 153876780401.dkr.ecr.us-east-2.amazonaws.com/covid-19-response/temperature-tracker
docker pull 153876780401.dkr.ecr.us-east-2.amazonaws.com/covid-19-response/temperature-tracker:$1