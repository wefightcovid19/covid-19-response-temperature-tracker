#!/bin/bash

cd ..
./mvnw clean package -Dnative -Dquarkus.native.container-build=true
docker build -f src/main/docker/Dockerfile.native -t 153876780401.dkr.ecr.us-east-2.amazonaws.com/covid-19-response/temperature-tracker .
